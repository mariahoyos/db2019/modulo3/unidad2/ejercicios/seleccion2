<?php
use yii\helpers\Html;

echo Html::a('Ciclistas', ['ciclista/index'], ['class' => 'btn btn-primary btn-large']);

echo Html::a('Equipo', ['equipo/index'], ['class' => 'btn btn-primary btn-large']);

echo Html::a('Etapa', ['etapa/index'], ['class' => 'btn btn-primary btn-large']);

echo Html::a('Lleva', ['lleva/index'], ['class' => 'btn btn-primary btn-large']);

echo Html::a('Maillot', ['maillot/index'], ['class' => 'btn btn-primary btn-large']);

echo Html::a('Puerto', ['puerto/index'], ['class' => 'btn btn-primary btn-large']);

?>
